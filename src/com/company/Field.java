package com.company;

public class Field {
    private final int[][] FIELD = new int[3][3];

    public synchronized void drawField(){
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(FIELD[i][j]);
            }
            System.out.println();
        }
    }

    public void setSign(int x, int y, char sign){
               if (FIELD[x][y] == 0){
                    FIELD[x][y] = sign;
                }
                else {
                    throw new IncorrectTurnException();
                }
                drawField();
            
        }
}
