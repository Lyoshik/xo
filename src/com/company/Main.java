package com.company;

public class Main {

    public static void main(String[] args) {
        Field field = new Field();
        Players vasya = new Players('X', field);
        Players petya = new Players('O', field);
        Thread vasyaThread = new Thread(vasya);
        Thread petyaThread = new Thread(petya);
        vasyaThread.start();
        petyaThread.start();
    }
}
