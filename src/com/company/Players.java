package com.company;

public class Players implements Runnable{
    int firstSide;
    int secondSide;
    Field field;
    char sign;

    public Players(char sign, Field field){
        if (sign == 'X'){
            firstSide = 1;
            secondSide = 2;
        }
        else if (sign == 'O'){
            firstSide = 2;
            secondSide = 1;
        }
        else {
            firstSide = 0;
            secondSide = 0;
        }
        this.sign = sign;
        this.field = field;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                field.setSign(i, j, sign);
            }
            
        }

    }
}
